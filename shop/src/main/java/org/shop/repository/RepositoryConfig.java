package org.shop.repository;

import java.io.*;
import java.util.Properties;

import org.shop.repository.map.*;
import org.shop.repository.factory.UserRepositoryFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Nikita_Orlovetc on 8/11/2017.
 */
@Configuration
public class RepositoryConfig {
    @Bean
    public ProductRepository productRepository(){
        System.out.println("ConfigProdRep");
        return new ProductMapRepository();
    }

    @Bean
    public ProposalRepository proposalRepository(){
        System.out.println("ConfigPropRep");
        return new ProposalMapRepository();
    }

    @Bean
    public UserRepository userRepository() {
        System.out.println("ConfigUserRep");
        UserRepositoryFactory userRepositoryFactory = new UserRepositoryFactory();
        return userRepositoryFactory.createUserRepository();
    }

    @Bean
    public OrderRepository orderRepository() {
        FileInputStream fis;
        Properties property = new Properties();
        Long initialSequence = 0L;
        try {
            fis = new FileInputStream("src/main/resources/config.properties");
            property.load(fis);
            String stringInitialSequence = property.getProperty("initialSequence");
            initialSequence = Long.valueOf(stringInitialSequence);
            System.out.println("HOST: " + initialSequence);
        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }
        return new OrderMapRepository();
    }

    @Bean
    public ItemRepository itemRepository() {
        return new ItemMapRepository();
    }

    @Bean
    public SellerRepository sellerRepository(){
        System.out.println("ConfigSelRep");
        return new SellerMapRepository();
    }

}
