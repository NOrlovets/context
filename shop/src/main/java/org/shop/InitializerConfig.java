package org.shop;

import org.shop.api.ProductService;
import org.shop.api.impl.ProductServiceImpl;
import org.shop.data.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Map;

@Configuration
//@PropertySource("classpath:/src/main/resources/config.properties")
//@ConfigurationProperties
public class InitializerConfig {
    @Bean
    public ProposalInitializer proposalInitializer(){
        System.out.println("ConfigPropIN");
        ProposalInitializer proposalInitializer = new ProposalInitializer();
        return proposalInitializer;
    }

    @Bean
    public SellerInitializer sellerInitializer() {
        System.out.println("ConfigSelIN");
        SellerInitializer sellerInitializer = new SellerInitializer();
        return sellerInitializer;
    }


//    @ConfigurationProperties
//    @Bean
//    public Map<Long, String> sellerNames(Map<Long, String> sellerNames) {
//        return sellerNames;
//    }

    @Bean
    public DataInitializer dataInitializer() {
        System.out.println("ConfigDataIN");
        DataInitializer dataInitializer = new DataInitializer();
        //dataInitializer.initData();
        return dataInitializer;
    }
}
