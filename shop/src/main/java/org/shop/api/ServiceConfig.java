package org.shop.api;

import org.shop.api.impl.*;
import org.shop.repository.ProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Nikita_Orlovetc on 8/11/2017.
 */
@Configuration
public class ServiceConfig {


    @Bean
    public OrderService orderService(){
    return new OrderServiceImpl();
    }


    @Bean
    public SellerService sellerService(){
        System.out.println("ConfigSelServ");
        return new SellerServiceImpl();
    }

    @Bean
    public UserService userService(){
        return new UserServiceImpl();
    }


}
