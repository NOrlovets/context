package org.shop;

import java.util.*;

import org.shop.api.SellerService;
import org.shop.data.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * The Seller Initializer util class.
 */
@Component
public class SellerInitializer {

    /** The seller service. */
    @Autowired
    private SellerService sellerService;
    
    /** The seller names. */
    //@Autowired
    private Map<Long, String> sellerNames = new HashMap<>();
    SellerInitializer(){
        sellerNames.put(1L,"hhh");
        sellerNames.put(2L,"hhh");
    }



    /**
     * Inits the sellers.
     */
    @PostConstruct
    public void initSellers() {
        System.out.println("SelIn");
        List<Seller> sellers = new LinkedList<Seller>();

        for (Map.Entry<Long, String> entry : sellerNames.entrySet()) {
            Seller seller = new Seller();
            seller.setId(entry.getKey());
            seller.setName(entry.getValue());
            
            sellers.add(seller);
        }
        
        sellerService.importSellers(sellers);
    }
}
