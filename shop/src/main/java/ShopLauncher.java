import org.shop.api.OrderService;
import org.shop.api.ProductService;
import org.shop.data.Item;
import org.shop.data.Order;
import org.shop.data.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by Nikita_Orlovetc on 8/12/2017.
 */
public class ShopLauncher {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(MainConfig.class);
        OrderService orderService = ctx.getBean(OrderService.class);
        orderService.createOrder(new User(), new Item());
        Order order = orderService.getOrderById(1L);
        System.out.println(order);
        ProductService productService = (ProductService) ctx.getBean("productService");
        System.out.println(productService.getProductById(1L));
    }
}
