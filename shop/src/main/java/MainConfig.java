import org.shop.ProductInitializer;
import org.shop.UserInitializer;
import org.shop.api.ItemService;
import org.shop.api.ProductService;
import org.shop.api.ProposalService;
import org.shop.api.UserService;
import org.shop.api.impl.ItemServiceImpl;
import org.shop.api.impl.ProductServiceImpl;
import org.shop.api.impl.ProposalServiceImpl;
import org.shop.repository.ItemRepository;
import org.shop.repository.ProductRepository;
import org.shop.repository.ProposalRepository;
import org.shop.repository.map.ProductMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by Nikita_Orlovetc on 8/11/2017.
 */
@Configuration
@Import({org.shop.InitializerConfig.class, org.shop.api.ServiceConfig.class, org.shop.repository.RepositoryConfig.class,
        org.shop.repository.factory.FactoryConfig.class})
public class MainConfig {
    @Autowired
    ItemRepository itemRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProposalRepository proposalRepository;

    @Autowired
    ProductService productService;

    @Autowired
    UserService userService;

    @Bean
    public ProductService productService() {
        System.out.println("ConfigProdServ");
        return new ProductServiceImpl(productRepository);
    }

    @Bean
    public ProposalService proposalService() {
        System.out.println("ConfigPropServ");
        System.out.println(proposalRepository);
        return new ProposalServiceImpl(proposalRepository);
    }

    @Bean
    public ProductInitializer productInitializer() {
        System.out.println("ConfigProdIn");
        ProductInitializer productInitializer = new ProductInitializer(productService);
        //return new ProductInitializer(productService);
        return productInitializer;
    }

    @Bean
    public UserInitializer userInitializer(){
        System.out.println("USERINI");
        UserInitializer userInitializer = new UserInitializer(userService);
        return userInitializer;
    }

    @Bean
    public ItemService itemService(){
        return new ItemServiceImpl(itemRepository);
    }
}
